provider "aws" {
  region     = "us-east-1"

  # Adicionando 'AWS_ACCESS_KEY_ID' e 'AWS_SECRET_ACCESS_KEY' nas environments do OS,
  # automaticamente ele as usa

  # Outra maneira dele usar as credenciais
  # shared_credentials_file = "/home/renandpf/.aws/credentials"
  # profile                 = "default"
}

resource "aws_s3_bucket" "b" {
  bucket = "my-tf-test-bucket-renandpf"
  acl    = "private"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
