# Mostra o plano de execução
terraform plan

# Executa o plano. Pode criar recursos ou alterar
terraform apply

# Mostra a infra
terraform show

# Deleta a infra
terraform destroy
