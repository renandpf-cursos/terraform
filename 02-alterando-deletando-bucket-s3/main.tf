provider "aws" {
  region     = "us-east-1"

  # Adicionando 'AWS_ACCESS_KEY_ID' e 'AWS_SECRET_ACCESS_KEY' nas environments do OS,
  # automaticamente ele as usa

  # Outra maneira dele usar as credenciais
  # shared_credentials_file = "/home/renandpf/.aws/credentials"
  # profile                 = "default"
}

# O nome do bucket foi o mesmo. E por isso ele vai alterar (pois mudei somente as tags)
resource "aws_s3_bucket" "b" {
  bucket = "my-tf-test-bucket-renandpf"
  acl    = "private"

  tags = {
    Name        = "My Super bucket"
    Environment = "Prod"
  }
}
